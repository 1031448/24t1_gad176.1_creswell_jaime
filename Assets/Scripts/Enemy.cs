using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;
using static UnityEditor.ShaderGraph.Internal.KeywordDependentCollection;
/// <summary>
/// The lazy enemy script, does nothing special
/// </summary>
public class Enemy : MonoBehaviour
{
    [SerializeField] public float movementSpeed = 5f;
    [SerializeField] public GameObject player;
    [SerializeField] private Vector3 playerPosition;

    protected virtual void Update()
    {
        // These next two lines is what makes the Lazy Enemy be able to find find the player through the attached gameObject,
        // and then move to what ever position the player is at at what ever speed you set in the inspector.
        playerPosition = player.transform.position;

        transform.position = Vector3.MoveTowards(transform.position, playerPosition, Time.deltaTime * movementSpeed);

       // this enables both enemies to rotate towards the player and look smooth
        Vector2 direction = player.transform.position - transform.position;
        direction.Normalize();
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(Vector3.forward * angle);
    }

    

}
