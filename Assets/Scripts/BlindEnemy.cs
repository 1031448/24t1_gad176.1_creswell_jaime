using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class BlindEnemy : Enemy
{
    //Distance is used for distance between the object and the player
    [SerializeField]private float distance;

    //my Render2 is used to highlight the specific object so it can be turned invisible
    [SerializeField]private Renderer myRenderer2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected void Update()
    {
        //this is used to rotate the object to face towards the player, also ads in the float for angle
        distance = Vector2.Distance(transform.position, player.transform.position);
        Vector2 direction = player.transform.position - transform.position;
        direction.Normalize();
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        //if the player is far away from the object/enemy then the object will become invisible and move at a slow pace towards the player
        if (distance > 4)
        {
            myRenderer2.enabled = false;
            movementSpeed = 1;
            transform.position = Vector2.MoveTowards(this.transform.position, player.transform.position, movementSpeed * Time.deltaTime);
            transform.rotation = Quaternion.Euler(Vector3.forward * angle);
        }
        //This is for when the player get within detection range of the enemy causing it to turn on and cause it to move at a faster speed
        else 
        {
            myRenderer2.enabled = true;
            movementSpeed = 4;
            transform.position = Vector2.MoveTowards(this.transform.position, player.transform.position, movementSpeed * Time.deltaTime);
            transform.rotation = Quaternion.Euler(Vector3.forward * angle);
        }
    }


}
